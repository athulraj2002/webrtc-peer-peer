var express = require('express')
//  var routes = require('./routes');

var app = module.exports = express.createServer();

var io = require('socket.io')(app);
// app.use('/', routes);
io.on('connection', (socket)=>{
	// console.log(socket)
	io.sockets.emit("user-joined", socket.id, io.engine.clientsCount, Object.keys(io.sockets.clients().sockets));

	socket.on('signal', (toId, message) => {
		io.to(toId).emit('signal', socket.id, message);
  	});

    socket.on("message", (data)=>{
		io.sockets.emit("broadcast-message", socket.id, data);
    })

	socket.on('disconnect', ()=> {
		io.sockets.emit("user-left", socket.id);
	})
});

app.listen(3000, ()=>{
	console.log("Express server listening on port %d in %s mode", app.address().port, app.settings.env);
});